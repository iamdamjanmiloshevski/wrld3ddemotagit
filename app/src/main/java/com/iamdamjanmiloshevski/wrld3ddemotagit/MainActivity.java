package com.iamdamjanmiloshevski.wrld3ddemotagit;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.eegeo.indoors.IndoorMapView;
import com.eegeo.mapapi.EegeoApi;
import com.eegeo.mapapi.EegeoMap;
import com.eegeo.mapapi.MapView;
import com.eegeo.mapapi.camera.CameraAnimationOptions;
import com.eegeo.mapapi.camera.CameraPosition;
import com.eegeo.mapapi.camera.CameraUpdateFactory;
import com.eegeo.mapapi.geometry.LatLng;
import com.eegeo.mapapi.indoors.IndoorMap;
import com.eegeo.mapapi.indoors.OnIndoorEnteredListener;
import com.eegeo.mapapi.indoors.OnIndoorExitedListener;
import com.eegeo.mapapi.map.OnInitialStreamingCompleteListener;
import com.eegeo.mapapi.map.OnMapReadyCallback;
import com.eegeo.mapapi.markers.MarkerOptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.mapView)
    MapView mMapView;
    @BindView(R.id.bt_top_floor)
    Button mTopFloor;
    @BindView(R.id.bt_move_down)
    Button mMoveDown;
    @BindView(R.id.bt_move_up)
    Button mMoveUp;
    @BindView(R.id.bt_bottom_floor)
    Button mBottomFloor;
    @BindView(R.id.bt_exit)
    Button mExit;
    @BindView(R.id.bt_golden_gate)
    Button mGoldenGate;
    private EegeoMap mEegeoMap = null;
    private IndoorMapView mIndoorMapView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        EegeoApi.init(this, getResources().getString(R.string.api_key));
        //setLockedOrientation();
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mMapView.onCreate(savedInstanceState);
        final List<Button> buttons = new ArrayList<>();
        buttons.add(mTopFloor);
        buttons.add(mMoveDown);
        buttons.add(mMoveUp);
        buttons.add(mBottomFloor);
        buttons.add(mExit);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final EegeoMap map) {
                IndoorEventListener listener = new IndoorEventListener(buttons);
                map.addOnIndoorEnteredListener(listener);
                map.addOnIndoorExitedListener(listener);
                mEegeoMap = map;
                RelativeLayout uiContainer = findViewById(R.id.eegeo_ui_container);
                mIndoorMapView = new IndoorMapView(mMapView, uiContainer, mEegeoMap);
                mEegeoMap.addMarker(new MarkerOptions().
                        position(new LatLng(37.769773, -122.466728))
                        .indoor("california_academy_of_sciences_19794", 0)
                        .labelText("Academy cafe"));
                map.addInitialStreamingCompleteListener(new OnInitialStreamingCompleteListener() {
                    @Override
                    public void onInitialStreamingComplete() {
                        CameraPosition position = new CameraPosition.Builder()
                                .target(37.769706, -122.4667133)
                                .indoor("california_academy_of_sciences_19794", 0)
                                .zoom(18)
                                .bearing(200) //Sets the orientation of the camera in the earth tangent plane, in degrees clockwise from north.
                                .build();

                        CameraAnimationOptions animationOptions = new CameraAnimationOptions.Builder()
                                .build();
                        map.animateCamera(CameraUpdateFactory.newCameraPosition(position), animationOptions);
                    }
                });
            }
        });
    }

    public void onClick(View view) {
        boolean mIndoors = false;
        if (mIndoors) {
            mEegeoMap.exitIndoorMap();
        } else {
            LatLng indoorMapLatLng = new LatLng(37.769506, -122.4667133);
            mEegeoMap.moveCamera(CameraUpdateFactory.newLatLng(indoorMapLatLng));
        }
    }

    public void onTopFloor(View view) {
        IndoorMap indoorMap = mEegeoMap.getActiveIndoorMap();
        mEegeoMap.setIndoorFloor(indoorMap.floorCount - 1);
    }

    public void onMoveUp(View view) {
        mEegeoMap.moveIndoorUp();
    }

    public void onMoveDown(View view) {
        mEegeoMap.moveIndoorDown();
    }

    public void onBottomFloor(View view) {
        mEegeoMap.setIndoorFloor(0);
    }

    public void onExit(View view) {
        mEegeoMap.exitIndoorMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    public void onGoldenGate(View view) {
        startActivity(new Intent(MainActivity.this, GoldenGateActivity.class));
    }

    public class IndoorEventListener implements OnIndoorEnteredListener, OnIndoorExitedListener {
        private List<Button> m_buttons;

        IndoorEventListener(List<Button> buttons) {
            this.m_buttons = buttons;
        }

        @Override
        public void onIndoorEntered() {
            IndoorMap indoorMap = mEegeoMap.getActiveIndoorMap();
            String indoorMapId = indoorMap.id;
            int indoorMapFloors = indoorMap.floorCount;
            Log.d("INDOOR MAP", "FLOORS: " + indoorMap.floorCount);
            Log.d("INDOOR MAP", indoorMapId);
            SetButtonStates(true);
        }

        @Override
        public void onIndoorExited() {
            SetButtonStates(false);
        }

        private void SetButtonStates(boolean state) {
            for (Iterator<Button> i = m_buttons.iterator(); i.hasNext(); ) {
                Button b = i.next();
                b.setEnabled(state);
            }
        }
    }
}

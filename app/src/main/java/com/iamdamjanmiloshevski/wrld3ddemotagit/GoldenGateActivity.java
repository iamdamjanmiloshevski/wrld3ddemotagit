package com.iamdamjanmiloshevski.wrld3ddemotagit;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.eegeo.mapapi.EegeoApi;
import com.eegeo.mapapi.EegeoMap;
import com.eegeo.mapapi.MapView;
import com.eegeo.mapapi.camera.CameraPosition;
import com.eegeo.mapapi.camera.CameraUpdateFactory;
import com.eegeo.mapapi.geometry.LatLng;
import com.eegeo.mapapi.map.OnInitialStreamingCompleteListener;
import com.eegeo.mapapi.map.OnMapReadyCallback;
import com.eegeo.mapapi.markers.Marker;
import com.eegeo.mapapi.markers.MarkerOptions;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoldenGateActivity extends AppCompatActivity {
    @BindView(R.id.mapView)
    MapView mMapView;
    private EegeoMap mEegeoMap;
    private Marker marker = null;
    private boolean mLocationToggle = false;
    private ArrayList<LatLng> locations = new ArrayList<>();
    private Handler mTimerHandler = new Handler();
    private LatLng posA = new LatLng(37.819164, -122.478909);
    private LatLng posB = new LatLng(37.821346, -122.479101);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        EegeoApi.init(this, getResources().getString(R.string.api_key));
        setContentView(R.layout.activity_golden_gate);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        getCarLocations(locations);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(EegeoMap map) {
                mEegeoMap = map;
                marker = mEegeoMap.addMarker(new MarkerOptions()
                        .position(posA)
                        .labelText("Car"));
                mEegeoMap.addInitialStreamingCompleteListener(new OnInitialStreamingCompleteListener() {
                    @Override
                    public void onInitialStreamingComplete() {
                        CameraPosition position = new CameraPosition.Builder()
                                .target(posA)
                                .zoom(16)
                                .bearing(270)
                                .build();
                        mEegeoMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 5000);
                    }
                });
                mTimerHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mEegeoMap != null) {
                            mLocationToggle = !mLocationToggle;
                            LatLng newLocation = mLocationToggle ? posB : posA;
                            marker.setPosition(newLocation);
                            mTimerHandler.postDelayed(this, TimeUnit.SECONDS.toMillis(2));
                        }
                    }
                }, TimeUnit.SECONDS.toMillis(2));
            }
        });
    }

    private void getCarLocations(ArrayList<LatLng> locations) {
        locations.add(new LatLng(37.819164, -122.478909));
        locations.add(new LatLng(37.818840, -122.478888));
        locations.add(new LatLng(37.819422, -122.478940));
        locations.add(new LatLng(37.819719, -122.478906));
        locations.add(new LatLng(37.820674, -122.479120));
        locations.add(new LatLng(37.820961, -122.479098));
        locations.add(new LatLng(37.821346, -122.479101));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

}
